<?php
/**
 * Rola.me API integration
 *
 * @author Gamaliel Espinoza M.
 * March 27th, 2014
 */
class Rolame {

  private
    $api_url = "http://rola.me/api",
    $api_key = '', $api_secret = '';

  public
    $error = null,
    $errorCode = 0;

  function __construct($options) {
    if(!isset($options['key']) or !isset($options['secret'])) {
      throw new UnexpectedValueException();
    }
    if(isset($options['api_url'])) {
      $this->api_url = $options['api_url'];
    }
    $this->api_key = $options['key'];
    $this->api_secret = $options['secret'];
  }

  function shorten($url) {
    $client = curl_init();
    curl_setopt($client, CURLOPT_POST, 1);
    curl_setopt($client, CURLOPT_URL, "{$this->api_url}/shorten");
    curl_setopt($client, CURLOPT_POSTFIELDS, 'url='.urlencode($url));
    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($client, CURLOPT_HTTPHEADER, [
      "X-Rolame-Key: {$this->api_key}",
      "X-Rolame-Secret: {$this->api_secret}",
    ]);
    $content = curl_exec($client);
    $httpCode = curl_getinfo($client, CURLINFO_HTTP_CODE);
    if($httpCode === 200) {
      return $content;
    } else {
      $this->errorCode = $content;
      return false;
    }
  }

}