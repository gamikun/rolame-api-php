# rola.me php api integration
You can do it this way:

    <?php
    require_once 'rolame/rolame.php';

    $rolame = new Rolame(array(
      'key' => 'aaa',
      'secret' => 'bbb',
    ));

    if($token = $rolame->shorten('http://mexico.com')) {
      echo $token;
    } else {
      echo "Error: " . $rolame->errorCode;
    }

Very simple.

