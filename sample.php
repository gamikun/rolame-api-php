<?php

require_once 'rolame/rolame.php';

$rolame = new Rolame([
  'key' => 'aaa',
  'secret' => 'bbb',
]);

if($url = $rolame->shorten('http://mexico.com')) {
  echo $url."\n";
} else {
  if($rolame->errorCode === 'invalid-url') {
    echo 'Invalid URL';
  } else {
    echo $rolame->errorCode;
  }
}
